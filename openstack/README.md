Ce code Terraform deploit un cluster Kubernetes sur Openstack grâce à [RKE2](https://docs.rke2.io/).
Le cluster comporte un nœud controlplane, un nœud edge pour le trafic applicatif et deux workers.

nginx-ingess-controler tourne sur le nœud edge et est configuré pour rediriger les trafic des ports 8000 et 8001 vers les services `default/svc1` et `default/svc2`.
