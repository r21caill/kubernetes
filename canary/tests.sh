#!/bin/bash

printf "No headers\n"
for i in $(seq 1 10);
do
curl -s --resolve echo.imt-atlantique.fr:80:127.0.0.1 echo.imt-atlantique.fr | grep "Hostname";
done

printf "\ncanary: never\n"
for i in $(seq 1 10);
do
curl -s -H "canary: never" --resolve echo.imt-atlantique.fr:80:127.0.0.1 echo.imt-atlantique.fr | grep "Hostname";
done

printf "\ncanary: always\n"
for i in $(seq 1 10);
do
curl -s -H "canary: always" --resolve echo.imt-atlantique.fr:80:127.0.0.1 echo.imt-atlantique.fr | grep "Hostname";
done
