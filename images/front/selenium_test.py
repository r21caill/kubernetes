import os
import uuid
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver import Remote
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import WebDriverException


"""
Frontend testing
run me with `python manage.py test front.selenium_test.FrontTests`
"""


class FrontTests(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        if 'LIVE_SERVER_PORT' in os.environ:
            cls.port = int(os.environ['LIVE_SERVER_PORT'])
        if 'TEST_URL' in os.environ:
            cls.live_server_url = os.environ['TEST_URL']
        else:
            super(FrontTests, cls).setUpClass()
        if 'SELENIUM_REMOTE' in os.environ:
            cls.selenium = Remote(
                os.environ['SELENIUM_REMOTE'],
                desired_capabilities=DesiredCapabilities.FIREFOX)
        else:
            cls.selenium = WebDriver()

    @classmethod
    def tearDownClass(self):
        self.selenium.quit()
        if 'TEST_URL' not in os.environ:
            super().tearDownClass()

    def test_01_new_point(self):
        name = str(uuid.uuid4())
        self.selenium.get(self.live_server_url)
        self.selenium.find_element_by_id('addlat').send_keys(
            '48.70333168603693')
        self.selenium.find_element_by_id('addlng').send_keys(
            '2.1348425745964055')
        self.selenium.find_element_by_id('addname').send_keys(
            name)
        self.selenium.find_element_by_id('adddate').send_keys(
            '2019-10-01')
        self.selenium.find_element_by_id('addpoint').click()
        self.selenium.\
            find_element_by_xpath(
                '//img[contains(@alt, "{}")]'.format(name)).\
            click()
        url = self.selenium.find_element_by_class_name(
            'delete-point').get_attribute('pointurl')
        self.selenium.get(url)

    def test_02_delete_point(self):
        name = str(uuid.uuid4())
        self.selenium.get(self.live_server_url)
        self.selenium.find_element_by_id('addlat').send_keys(
            '48.70333168603693')
        self.selenium.find_element_by_id('addlng').send_keys(
            '2.1348425745964055')
        self.selenium.find_element_by_id('addname').send_keys(
            name)
        self.selenium.find_element_by_id('adddate').send_keys(
            '2019-10-01')
        self.selenium.find_element_by_id('addpoint').click()
        self.selenium.\
            find_element_by_xpath(
                '//img[contains(@alt, "{}")]'.format(name)).\
            click()
        url = self.selenium.find_element_by_class_name(
            'delete-point').get_attribute('pointurl')
        self.selenium.find_element_by_class_name(
            'delete-point').click()
        self.assertRaises(WebDriverException, self.selenium.get(url))
